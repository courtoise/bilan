﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Globalization;

namespace CourtoiseBilan
{
    public static class TableWriter
    {
        public static byte[] Write(XTable table)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var p = new ExcelPackage())
            {
                foreach (var sheet in table.Sheets)
                {
                    var s = p.Workbook.Worksheets.Add(sheet.Title);
                    var extras = sheet.Extras?.Select((x, i) => new { Key = i, Value = x });
                    var currentRow = sheet.RowStart;
                    s.Cells.Style.Numberformat.Format = "# ##0";

                    // Sheet compagny
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Merge = true;
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Value = table.Company;
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Style.Font.Bold = true;
                    currentRow++;

                    // Sheet title
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Merge = true;
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Value = sheet.Title;
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Style.Font.Size = 14;
                    s.Cells[sheet.RowStart, sheet.ColStart, currentRow, sheet.ColEnd - 4].Style.Font.Bold = true;
                    // Sheet date
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Merge = true;
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Value = table.Date.ToShortDateString();
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    s.Cells[currentRow, sheet.ColEnd - 3, currentRow, sheet.ColEnd].Style.Font.Bold = true;
                    currentRow++;

                    foreach (var group in sheet.Groups)
                    {
                        // Group header
                        s.Cells[currentRow, 1].Value = group.Name.ToUpper();
                        foreach (var monthIndex in Enumerable.Range(1, 12))
                        {
                            s.Cells[currentRow, monthIndex + 1].Value = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(monthIndex);
                        }
                        s.Cells[currentRow, 14].Value = "total";
                        s.Cells[currentRow, 15].Value = "%";
                        if (extras != null)
                        {
                            foreach (var extra in extras)
                            {
                                if (group.ExtrasDisabled?[extra.Key] != true)
                                    s.Cells[currentRow, 16 + extra.Key].Value = extra.Value.Name;
                            }
                        }
                        s.Cells[currentRow, 1, currentRow, sheet.ColEnd].Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                        s.Cells[currentRow, 1, currentRow, sheet.ColEnd].Style.Fill.BackgroundColor.SetColor(Color.Orange);
                        s.Cells[currentRow, 1, currentRow, sheet.ColEnd].Style.Font.Bold = true;
                        currentRow++;

                        var totalAddress = s.Cells[currentRow, 2, currentRow + group.Items.Count() - 1, 13].Address;
                        foreach (var item in group.Items)
                        {
                            s.Cells[currentRow, 1].Value = item.Name.ToUpper();
                            foreach (var value in item.Values.Select((x, i) => new { Index = i, Value = x }))
                            {
                                s.Cells[currentRow, value.Index + 2].Value = value.Value;
                            }
                            var rowAddress = s.Cells[currentRow, 2, currentRow, 13].Address;
                            s.Cells[currentRow, 14].Formula = $"=SUM({rowAddress})";
                            s.Cells[currentRow, 15].Formula = $"=SUMPRODUCT(ABS({rowAddress}))/SUMPRODUCT(ABS({totalAddress}))";
                            s.Cells[currentRow, 15].Style.Numberformat.Format = "#0%";
                            if (item.ExtraValues != null)
                            {
                                foreach (var extra in item.ExtraValues.Select((x, i) => new { Key = i, Value = x }))
                                {
                                    if (group.ExtrasDisabled?[extra.Key] != true)
                                    {
                                        s.Cells[currentRow, 16 + extra.Key].Value = extra.Value;
                                        if (sheet.Extras[extra.Key].Decimal)
                                            s.Cells[currentRow, 16 + extra.Key].Style.Numberformat.Format = "# ##0.00";
                                    }
                                }
                            }
                            currentRow++;
                        }
                        s.Cells[currentRow, 1].Value = "TOTAL";

                        s.Cells[currentRow - group.Items.Count(), 14, currentRow, 14].Style.Fill.PatternType = ExcelFillStyle.DarkGray;
                        s.Cells[currentRow - group.Items.Count(), 14, currentRow, 14].Style.Fill.BackgroundColor.SetColor(Color.Yellow);

                        foreach (var col in Enumerable.Range(2, 12))
                        {
                            s.Cells[currentRow, col].Formula = $"=SUM({ s.Cells[currentRow - group.Items.Count(), col, currentRow - 1, col].Address })";
                        }
                        s.Cells[currentRow, 14].Formula = $"=SUM({ s.Cells[currentRow - group.Items.Count(), 2, currentRow - 1, 13].Address })";
                        if (extras != null)
                        {
                            foreach (var extra in extras)
                            {
                                if (extra.Value.Total)
                                {
                                    var col = 16 + extra.Key;
                                    s.Cells[currentRow, col].Formula = $"=SUM({ s.Cells[currentRow - group.Items.Count(), col, currentRow - 1, col].Address })";
                                }
                            }
                        }
                        s.Cells[currentRow, sheet.ColStart, currentRow, sheet.ColEnd].Style.Font.Color.SetColor(Color.Red);
                        s.Cells[currentRow, sheet.ColStart, currentRow, sheet.ColEnd].Style.Font.Bold = true;

                        currentRow++;
                    }
                    s.Cells.AutoFitColumns();
                    s.PrinterSettings.PaperSize = ePaperSize.A4;
                    s.PrinterSettings.Orientation = eOrientation.Landscape;
                    s.PrinterSettings.HorizontalCentered = true;
                    s.PrinterSettings.TopMargin = 0.75m;
                    s.PrinterSettings.BottomMargin = 0.75m;
                    s.PrinterSettings.LeftMargin = 0.25m;
                    s.PrinterSettings.RightMargin = 0.25m;
                    s.PrinterSettings.FitToPage = true;
                    s.PrinterSettings.FitToHeight = 0;
                    s.PrinterSettings.FitToWidth = 1;
                }

                return p.GetAsByteArray();
            }
        }
    }
}
