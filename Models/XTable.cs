﻿namespace CourtoiseBilan
{
    public class XTable
    {
        public string FileName;
        public string Company = "La Courtoise ressourcerie";
        public DateTime Date = DateTime.Now;
        public XSheet[] Sheets;
    }
}
