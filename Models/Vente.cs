﻿namespace CourtoiseBilan
{
    public class Vente : Categorisable
    {
        public double Price;
        public double Quantity;
        public double Weight;
    }

    public static class VenteExtensions
    {
        public static List<Vente> Merge(this IEnumerable<IGrouping<DateTime, Vente>> ventes)
        {
            return ventes.Select(x => new Vente
            {
                Date = x.Key.Date,
                Category = -1,
                SubCategory = -1,
                Weight = x.Sum(y => y.Weight),
                Price = x.Sum(y => y.Price),
                Quantity = x.Sum(y => y.Quantity),
            }).ToList();
        }

        public static XItem ToXItem(this IEnumerable<Vente> ventes, string name, Func<Vente, bool> filter)
        {
            var mergedVentes = ventes.GroupByCategory(filter).Merge();
            return new XItem()
            {
                Name = name,
                Values = mergedVentes.ToXItemValues(),
                ExtraValues = mergedVentes.ToXItemExtras()
            };
        }

        public static double[] ToXItemExtras(this IEnumerable<Vente> ventes)
        {
            var q = ventes.Sum(x => x.Quantity);
            var p = ventes.Sum(x => x.Weight);
            var pm = ventes.Sum(x => x.Price) / q;
            return new[] { p, q, pm };
        }

        public static double[] ToXItemValues(this IEnumerable<Vente> ventes) =>
            Enumerable.Range(1, 12).Select(x =>
                    ventes.SingleOrDefault(y => y.Date.Month == x)?.Price ?? 0).ToArray();

        public static XGroup ToXGroupByCategory(this IEnumerable<Vente> ventes, long category, string name) => new XGroup
        {
            Name = name ?? DAO.Instance.GetCategorieNames()[category],
            Items = ventes.Where(x => x.Category == category).GroupBy(x => x.SubCategory).Select(x => new XItem
            {
                Name = DAO.Instance.GetSubCategorieNames()[x.Key],
                Values = x.ToList().ToXItemValues(),
                ExtraValues = x.ToList().ToXItemExtras()
            }).ToArray()
        };
    }
}
