﻿using System.Data;

namespace CourtoiseBilan
{
    public class Flux : Categorisable
    {
        public double Weight;
    }

    public static class FluxExtensions
    {
        public static List<Flux> Merge(this List<IGrouping<DateTime, Flux>> fluxes)
        {
            return fluxes.Select(x => new Flux
            {
                Date = x.Key.Date,
                Category = -1,
                SubCategory = -1,
                Weight = x.Sum(y => y.Weight)
            }).ToList();
        }

        public static XItem ToXItem(this List<Flux> fluxes, string name, Func<Flux, bool> filter)
        {
            var mergedFlux = fluxes.GroupByCategory(filter).Merge();

            return new XItem()
            {
                Name = name,
                Values = mergedFlux.ToXItemValues()
            };
        }

        public static double[] ToXItemValues(this List<Flux> fluxes) => 
            Enumerable.Range(1, 12).Select(x =>
                    fluxes.SingleOrDefault(y => y.Date.Month == x)?.Weight ?? 0).ToArray();

        public static XGroup ToXGroupByCategory(this List<Flux> fluxes, long category, string name, bool parTournee = false) => new XGroup
        {
            Name = name ?? DAO.Instance.GetCategorieNames()[category],
            Items = fluxes.Where(x => x.Category == category).GroupBy(x => x.SubCategory).Select(x => new XItem
            {
                Name = DAO.Instance.GetSubCategorieNames(parTournee)[x.Key],
                Values = x.ToList().ToXItemValues()
            }).ToArray()
        };
    }
}

