﻿namespace CourtoiseBilan
{
    static class SheetGenerator
    {
        private static readonly List<Category> Categories = new List<Category>()
        {
            new Category { ID = 1, Name = "Paniers" },
            new Category { ID = 13, Name = "Divers" },
            new Category { ID = 8, Name = "Gros électro menager" },
            new Category { ID = 10, Name = "Multimedia" },
            new Category { ID = 11, Name = "Outillage électro" },
            new Category { ID = 9, Name = "Petit électro menager" },
            new Category { ID = 12, Name = "Loisirs" },
            new Category { ID = 2, Name = "Maison" }
        };

        private static readonly List<Category> MobCategories = new List<Category>()
        {
            new Category { ID = 3, Name = "Assises" },
            new Category { ID = 4, Name = "Couchage" },
            new Category { ID = 5, Name = "Plan de travail" },
            new Category { ID = 6, Name = "Rangements" },
            new Category { ID = 7, Name = "Professionnels" }
        };

        private static readonly XExtra[] VenteExtras = new[]
        {
            new XExtra { Name = "poids", Total = true },
            new XExtra { Name = "nombre", Total = true },
            new XExtra { Name = "prix moyen", Decimal = true }
        };

        private static readonly long[] RelookingIds = new long[]
        {
            111,107,109,124
        };

        public static XSheet GetFlux(List<Flux> fluxes) => new XSheet
        {
            Title = $"Flux {Config.Data.Year} en kg",
            Groups = new[]
            {
                new XGroup()
                {
                    Name = "Flux entrants",
                    Items = new[]
                    {
                        fluxes.ToXItem("Apports volontaire", x => x.Category != 17 &&  x.Category != 21),
                        fluxes.ToXItem("Collectes", x => x.Category == 17),
                        fluxes.ToXItem("Débarras", x => x.Category == 21),
                    }
                },
                new XGroup()
                {
                    Name = "Flux sortants",
                    Items = new[]
                    {
                        fluxes.ToXItem("Reemploi", x => x.Category != 17 && x.Category != 18 && x.Category != 22),
                        fluxes.ToXItem("Recyclage", x => (x.Category == 18 && x.SubCategory != 119) || x.Category == 22),
                        fluxes.ToXItem("Rejet", x => x.SubCategory == 119),
                    }
                },
                new XGroup()
                {
                    Name = "Flux sortants détournés",
                    Items = new[]
                    {
                        fluxes.ToXItem("Flux entrants", x => (x.Category != 17 &&  x.Category != 21) || x.Category == 17 || x.Category == 21),
                        fluxes.ToXItem("Rejet", x => x.SubCategory == 119).Negative(),
                    }
                },
                new XGroup()
                {
                    Name= "Reemploi",
                    Items = Categories
                        .Select(x => fluxes.ToXItem(x.Name, y => y.Category == x.ID))
                        .Concat(new[]{ fluxes.ToXItem("Mobilier", x => MobCategories.Select(y => y.ID).Contains(x.Category)) }).ToArray()
                },
                new XGroup()
                {
                    Name = "Recyclage",
                    Items = new[]
                    {
                        fluxes.ToXItem("Ecologic (DEE)", x => x.SubCategory == 121),
                        fluxes.ToXItem("Ecomobilier (mobilier)", x => x.SubCategory == 123),
                        fluxes.ToXItem("Paprec (livres)", x => x.SubCategory == 122),
                        fluxes.ToXItem("Métaux", x => x.SubCategory == 120),
                        fluxes.ToXItem("Mobilier", x => x.Category == 22),
                        fluxes.ToXItem("Dechetterie (collectes)", x => x.Category == 17),
                    }
                },
                new XGroup()
                {
                    Name = "Rejet",
                    Items = new[]
                    {
                        fluxes.ToXItem("Déchetterie", x => x.SubCategory == 119),
                    }
                }
            }
        };

        public static XSheet GetFluxDetail(List<Flux> fluxes) => new XSheet
        {
            Title = $"Détail des flux {Config.Data.Year} en kg",
            Groups = Categories
                .Select(x => fluxes.ToXGroupByCategory(x.ID, x.Name))
                .Concat(new[]
                    {
                        new XGroup
                        {
                            Name = "Mobilier",
                            Items = MobCategories.Select(x => fluxes.ToXItem(x.Name, y => y.Category == x.ID)).ToArray()
                        }
                    })
                .Concat(MobCategories.Where(x => x.ID != 7).Select(y => fluxes.ToXGroupByCategory(y.ID, y.Name)))
                .ToArray()
        };

        public static XSheet GetCollectesTournee(List<Flux> fluxes) => new XSheet
        {
            Title = $"Collectes par commune {Config.Data.Year} en kg",
            Groups = new[]
            {
                fluxes.ToXGroupByCategory(17, "Collectes", true)
            }
        };

        public static XSheet GetVente(List<Vente> ventesWithRelooking)
        {
            List<Vente> ventes = ventesWithRelooking.Where(x => !RelookingIds.Contains(x.SubCategory)).ToList();
            return new XSheet()
            {
                Title = $"Ventes {Config.Data.Year} en €",
                Extras = VenteExtras,
                Groups = new[]
            {
                new XGroup()
                {
                    Name = "Ventes",
                    ExtrasDisabled = new[] {false, false, true},
                    Items = Categories
                        .Select(x => ventes.ToXItem(x.Name, y => y.Category == x.ID))
                        .Concat(new[]{
                            ventes.ToXItem("Mobilier", x => MobCategories.Select(y => y.ID).Contains(x.Category)),
                            ventesWithRelooking.ToXItem("Relooking", x => RelookingIds.Contains(x.SubCategory))
                        })
                        .ToArray()
                },
                ventes.ToXGroupByCategory(1, Categories.Single(x => x.ID == 1).Name),
                new XGroup
                {
                    Name = "Mobilier",
                    Items = MobCategories.Select(x => ventes.ToXItem(x.Name, y => y.Category == x.ID)).ToArray()
                }
            }
            };
        }

        public static XSheet GetVenteDetail(List<Vente> ventesWithRelooking)
        {
            List<Vente> ventes = ventesWithRelooking.Where(x => !RelookingIds.Contains(x.SubCategory)).ToList();
            List<Vente> relookingVents = ventesWithRelooking.Where(x => RelookingIds.Contains(x.SubCategory)).ToList();

            return new XSheet()
            {
                Title = $"Détail des ventes {Config.Data.Year} en €",
                Extras = VenteExtras,
                Groups = Categories
                    .Select(x => ventes.ToXGroupByCategory(x.ID, x.Name))
                    .Concat(MobCategories.Where(x => x.ID != 7).Select(y => ventes.ToXGroupByCategory(y.ID, y.Name)))
                    .Concat(new[] {
                        new XGroup {
                            Name = "Relooking",
                            Items = ventesWithRelooking
                                .Where(x => RelookingIds.Contains(x.SubCategory))
                                .GroupBy(x => x.Category)
                                .Select(x => x.ToXItem(Categories.Concat(MobCategories).Single(y => y.ID == x.Key).Name, _ => true))
                                .ToArray(),
                        }
                    })
                    .ToArray()
            };
        }
    }
}
