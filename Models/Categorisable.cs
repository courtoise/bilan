﻿namespace CourtoiseBilan
{
    public class Categorisable
    {
        public DateTime Date;
        public long Category;
        public long SubCategory;
    }

    public static class CategorisableExtenstions
    {
        public static List<IGrouping<DateTime, T>> GroupByCategory<T>(this IEnumerable<T> items, Func<T, bool> filter) where T : Categorisable
        {
            return items
                .Where(filter)
                .GroupBy(x => x.Date)
                .ToList();
        }
    }
}
