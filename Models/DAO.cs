﻿using System.Data;
using System.Data.OleDb;

namespace CourtoiseBilan
{
    class DAO : IDisposable
    {

        private readonly Dictionary<string, object> Cache = new Dictionary<string, object>();
        private static DAO? _instance;
        private readonly OleDbConnection connection;
        private readonly int year;
        protected DAO()
        {
            year = Config.Data.Year;
            connection = new OleDbConnection($@"Provider=PCSOFT.HFSQL;Data Source={Config.Data.Host};User ID={Config.Data.User};Password={Config.Data.Password};Initial Catalog=GDR");
        }

        public static DAO Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DAO();
                return _instance;
            }
        }

        public List<Flux> GetFluxes(long siteId, bool parTournee = false)
        {
            DataTable table = new DataTable();
            string requete, parTourneeRequete;
            if (siteId != 0)
            {
                requete = $@"
                SELECT SUM(a.Poids) as Poids, a.Date, a.IDSous_Catégorie, a.IDCatégorie, a.IDSite FROM (
                    SELECT Produit.Poids, Produit.IDSous_Catégorie, Produit.IDCatégorie, TRUNC(Arrivage.Date, 'MM') AS Date, Arrivage.IDSite
                    FROM Produit 
                        LEFT JOIN Arrivage ON Produit.IDArrivage == Arrivage.IDArrivage
                        LEFT JOIN Categorie ON Produit.IDCatégorie == Categorie.IDCatégorie
                    WHERE Categorie.Secteur_Collecte=1
                ) a 
                WHERE a.Date BETWEEN '{year}0101' AND '{year}1231' AND a.IDSite == '{siteId}'
                GROUP BY a.IDSite, a.Date, a.IDCatégorie, a.IDSous_Catégorie 
                ORDER BY a.Date, a.IDSous_Catégorie
            ";

                parTourneeRequete = $@"
                SELECT SUM(a.Poids) as Poids, a.Date, a.IDTournée AS IDSous_Catégorie, a.IDCatégorie, a.IDSite FROM (
                    SELECT Produit.Poids, Produit.IDSous_Catégorie, Produit.IDCatégorie, Arrivage.IDTournée, TRUNC(Arrivage.Date, 'MM') AS Date, Arrivage.IDSite
                    FROM Produit 
                        LEFT JOIN Arrivage ON Produit.IDArrivage == Arrivage.IDArrivage
                        LEFT JOIN Categorie ON Produit.IDCatégorie == Categorie.IDCatégorie
                    WHERE Categorie.Secteur_Collecte=1
                ) a 
                WHERE a.Date BETWEEN '{year}0101' AND '{year}1231' AND a.IDSite == '{siteId}'
                GROUP BY a.IDSite, a.Date, a.IDCatégorie, a.IDTournée 
                ORDER BY a.Date, a.IDSous_Catégorie
            ";
            }
            else
            {
                requete = $@"
                SELECT SUM(a.Poids) as Poids, a.Date, a.IDSous_Catégorie, a.IDCatégorie FROM (
                    SELECT Produit.Poids, Produit.IDSous_Catégorie, Produit.IDCatégorie, TRUNC(Arrivage.Date, 'MM') AS Date
                    FROM Produit 
                        LEFT JOIN Arrivage ON Produit.IDArrivage == Arrivage.IDArrivage
                        LEFT JOIN Categorie ON Produit.IDCatégorie == Categorie.IDCatégorie
                    WHERE Categorie.Secteur_Collecte=1
                ) a 
                WHERE a.Date BETWEEN '{year}0101' AND '{year}1231' 
                GROUP BY a.Date, a.IDCatégorie, a.IDSous_Catégorie 
                ORDER BY a.Date, a.IDSous_Catégorie
            ";

                parTourneeRequete = $@"
                SELECT SUM(a.Poids) as Poids, a.Date, a.IDTournée AS IDSous_Catégorie, a.IDCatégorie FROM (
                    SELECT Produit.Poids, Produit.IDSous_Catégorie, Produit.IDCatégorie, Arrivage.IDTournée, TRUNC(Arrivage.Date, 'MM') AS Date
                    FROM Produit 
                        LEFT JOIN Arrivage ON Produit.IDArrivage == Arrivage.IDArrivage
                        LEFT JOIN Categorie ON Produit.IDCatégorie == Categorie.IDCatégorie
                    WHERE Categorie.Secteur_Collecte=1
                ) a 
                WHERE a.Date BETWEEN '{year}0101' AND '{year}1231' 
                GROUP BY a.Date, a.IDCatégorie, a.IDTournée
                ORDER BY a.Date, a.IDSous_Catégorie
            ";
            }

            using (OleDbDataAdapter adapter = new OleDbDataAdapter(parTournee ? parTourneeRequete : requete, connection))
            {
                adapter.Fill(table);
            }

            return table.AsEnumerable().Select(x => new Flux
            {
                Weight = x.Field<double>("Poids"),
                Category = x.Field<long>("IDCatégorie"),
                SubCategory = x.Field<long>("IDSous_Catégorie"),
                Date = x.Field<DateTime>("Date")
            }).ToList();
        }

        public List<Vente> GetVentes(long siteId)
        {
            DataTable table = new DataTable();
            string requete;
            if (siteId != 0)
            {
                requete = $@"
                    SELECT a.IDCatégorie, a.IDSous_Catégorie, SUM(a.Montant) as Montant, SUM(a.Nombre) AS Nombre, SUM(a.Poids) as Poids, a.Date, a.IDSite
                    FROM ( 
	                    SELECT Lignes_Vente.IDCatégorie, Lignes_Vente.IDSous_Catégorie, Lignes_Vente.Montant, Lignes_Vente.Nombre, Lignes_Vente.Poids, TRUNC(Vente_Magasin.Date, 'MM') AS Date, Vente_Magasin.IDSite
	                    FROM Lignes_Vente
		                    LEFT JOIN Vente_Magasin ON Lignes_Vente.IDVente_Magasin == Vente_Magasin.IDVente_Magasin
	                    ) a
                    WHERE a.Date BETWEEN '{year}0101' AND '{year}1231' AND a.IDSite == '{siteId}'
                    GROUP BY a.IDSite, a.Date, a.IDCatégorie, a.IDSous_Catégorie
                    ORDER BY a.Date, a.IDSous_Catégorie
                ";
            }
            else
            {
                requete = $@"
                    SELECT a.IDCatégorie, a.IDSous_Catégorie, SUM(a.Montant) as Montant, SUM(a.Nombre) AS Nombre, SUM(a.Poids) as Poids, a.Date
                    FROM ( 
	                    SELECT Lignes_Vente.IDCatégorie, Lignes_Vente.IDSous_Catégorie, Lignes_Vente.Montant, Lignes_Vente.Nombre, Lignes_Vente.Poids, TRUNC(Vente_Magasin.Date, 'MM') AS Date
	                    FROM Lignes_Vente
		                    LEFT JOIN Vente_Magasin ON Lignes_Vente.IDVente_Magasin == Vente_Magasin.IDVente_Magasin
	                    ) a
                    WHERE a.Date BETWEEN '{year}0101' AND '{year}1231'
                    GROUP BY a.Date, a.IDCatégorie, a.IDSous_Catégorie
                    ORDER BY a.Date, a.IDSous_Catégorie
                ";
            }
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(requete, connection))
            {
                adapter.Fill(table);
            }

            return table.AsEnumerable().Select(x => new Vente
            {
                Price = Convert.ToDouble(x.Field<decimal>("Montant")),
                Quantity = x.Field<double>("Nombre"),
                Weight = x.Field<double>("Poids"),
                Category = x.Field<long>("IDCatégorie"),
                SubCategory = x.Field<long>("IDSous_Catégorie"),
                Date = x.Field<DateTime>("Date")
            }).ToList();

        }

        public Dictionary<long, long[]> GetSubCategories()
        {
            string cacheKey = "GetSubCategories";
            if (!Cache.ContainsKey(cacheKey))
            {
                DataTable table = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter($@"
                    SELECT IDCatégorie, IDSous_Catégorie FROM Sous_Categorie WHERE Secteur_Collecte=1
                ", connection))
                {
                    adapter.Fill(table);
                }

                Cache[cacheKey] = table.AsEnumerable()
                    .Select(x => new { Category = x.Field<long>("IDCatégorie"), SubCategory = x.Field<long>("IDSous_Catégorie") })
                    .GroupBy(x => x.Category)
                    .ToDictionary(x => x.Key, x => x.Select(y => y.SubCategory).ToArray());
            }
            return (Dictionary<long, long[]>)Cache[cacheKey];
        }

        public Dictionary<long, string> GetSubCategorieNames(bool parTournee = false)
        {
            string cacheKey = parTournee ? "GetSubCategorieNamesParTournee" : "GetSubCategorieNames";
            if (!Cache.ContainsKey(cacheKey))
            {
                DataTable table = new DataTable();

                var requete = $@"
                    SELECT IDSous_Catégorie, Désignation FROM Sous_Categorie
                ";

                var parTourneeRequete = $@"
                    SELECT Tournee.IDTournée as IDSous_Catégorie, Tournee.Intitulé AS Désignation FROM Tournee
                ";

                using (OleDbDataAdapter adapter = new OleDbDataAdapter(parTournee ? parTourneeRequete : requete, connection))
                {
                    adapter.Fill(table);
                }

                var dic = table.AsEnumerable()
                    .Select(x => new { SubCategory = x.Field<long>("IDSous_Catégorie"), Name = x.Field<string>("Désignation") })
                    .ToDictionary(x => x.SubCategory, x => x.Name);

                if (parTournee)
                {
                    dic.Add(0, "Autres");
                }

                Cache[cacheKey] = dic;
            }
            return (Dictionary<long, string>)Cache[cacheKey];
        }

        public Dictionary<long, string> GetCategorieNames()
        {
            string cacheKey = "GetCategorieNames";
            if (!Cache.ContainsKey(cacheKey))
            {
                DataTable table = new DataTable();
                using (OleDbDataAdapter adapter = new OleDbDataAdapter($@"
                    SELECT IDCatégorie, Désignation FROM Categorie
                ", connection))
                {
                    adapter.Fill(table);
                }

                Cache[cacheKey] = table.AsEnumerable()
                    .Select(x => new { SubCategory = x.Field<long>("IDCatégorie"), Name = x.Field<string>("Désignation") })
                    .ToDictionary(x => x.SubCategory, x => x.Name);
            }
            return (Dictionary<long, string>)Cache[cacheKey];
        }

        public Dictionary<long, string> GetSites()
        {
            DataTable table = new DataTable();
            using (OleDbDataAdapter adapter = new OleDbDataAdapter($@"
                    SELECT IDSite, Désignation FROM site;
                ", connection))
            {
                adapter.Fill(table);
            }

            return table.AsEnumerable()
                .Select(x => new { ID = x.Field<long>("IDSite"), Name = x.Field<string>("Désignation") })
                .ToDictionary(
                x => x.ID,
                x => x.Name);
        }

        public void Dispose()
        {
            connection.Close();
        }
    }
}
