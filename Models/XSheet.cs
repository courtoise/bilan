﻿namespace CourtoiseBilan
{
    public class XSheet
    {
        public string Title;
        public XGroup[] Groups;
        public XExtra[] Extras;

        public readonly int RowStart = 1;
        public int RowEnd {
            get => Groups.Sum(x => x.Items.Count() + 2) + 1;
        }

        public readonly int ColStart = 1;
        public int ColEnd
        {
            get => (1 + 12 + 2) + (Extras?.Length ?? 0);
        }
    }
}
