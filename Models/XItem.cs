﻿namespace CourtoiseBilan
{
    public class XItem
    {
        public string Name;
        public double[] Values;
        public double[] ExtraValues;
    }

    public static class XItemExtensions
    {
        public static XItem Negative(this XItem xItem)
        {
            xItem.Values = xItem.Values.Select(x => -x).ToArray();
            return xItem;
        }
    }
}
