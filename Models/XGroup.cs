﻿namespace CourtoiseBilan
{
    public class XGroup
    {
        public string Name;
        public XItem[] Items;
        public bool[] ExtrasDisabled;
    }
}
