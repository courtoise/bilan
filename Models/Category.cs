﻿namespace CourtoiseBilan
{
    public struct Category
    {
        public long ID;
        public string Name;
    }
}
