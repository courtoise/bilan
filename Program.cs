﻿namespace CourtoiseBilan
{
    internal class Program
    {
        public static void Main()
        {
            WriteTitle();
            var config = Config.Data;
            var updateConfig = config.IsBlank;
            if (!updateConfig)
            {
                Console.WriteLine("Voulez-vous changer la configuration ? (o/N)");
                var change = Console.ReadKey();
                Console.Clear();
                WriteTitle();
                updateConfig = change.Key == ConsoleKey.O;
            }
            if (updateConfig)
            {
                config.Host = GetConfig("Addresse de la base de données", config.Host);
                config.User = GetConfig("Utilisateur de la base de données", config.User);
                config.Password = GetConfig("Mot de passe de la base de données", config.Password);
                config.Year = int.Parse(GetConfig("Année", config.Year.ToString()));
                config.Save();
                Console.Clear();
                WriteTitle();
            }
            Console.WriteLine("Génration des bilans en cours...");

            try
            {
                var file = GenerateBilan(config);
                File.WriteAllBytes($"courtoise_bilan_{config.Year}.xls", file);
                var sites = DAO.Instance.GetSites();
                foreach (var site in sites)
                {
                    file = GenerateBilan(config, site.Key);
                    var name = site.Value.ToLowerInvariant().Replace(" ", "_");
                    File.WriteAllBytes($"courtoise_bilan_{config.Year}_{name}.xls", file);
                }
                Console.WriteLine("Génration des bilans fini.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Une erreur est survenue.");
            }
            Console.WriteLine("Appuyez sur une ECHAP pour quitter.");
            while (Console.ReadKey().Key != ConsoleKey.Escape) { }
            Environment.Exit(0);
        }

        static byte[] GenerateBilan(Config config, long siteId = 0)
        {
            List<Flux> fluxes = DAO.Instance.GetFluxes(siteId);
            List<Flux> fluxesParTournee = DAO.Instance.GetFluxes(siteId, true);
            List<Vente> ventes = DAO.Instance.GetVentes(siteId);

            XTable table = new XTable
            {
                FileName = $"courtoise_bilan_{config.Year}",
                Sheets = new[]
                {
                        SheetGenerator.GetFlux(fluxes),
                        SheetGenerator.GetCollectesTournee(fluxesParTournee),
                        SheetGenerator.GetFluxDetail(fluxes),
                        SheetGenerator.GetVente(ventes),
                        SheetGenerator.GetVenteDetail(ventes),
                    }
            };
            return TableWriter.Write(table);
        }

        static void WriteTitle()
        {
            Console.WriteLine($"[Courtoise Bilan]");
        }

        static string GetConfig(string label, string value)
        {
            Console.Write($"{label} ({value}) : ");
            var answer = Console.ReadLine();
            if (answer != null && answer != "")
            {
                return answer;
            }
            return value;
        }

    }
}