﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace CourtoiseBilan
{
    internal class Config
    {
        [JsonInclude]
        public string Host = "";
        [JsonInclude]
        public string User = "admin";
        [JsonInclude]
        public string Password = "";
        public int Year = DateTime.Now.Year;
        public bool IsBlank = true;
        public static string DIRECTORY = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CourtoiseBilan");
        public static string FILE = Path.Combine(DIRECTORY, "config.json");
        private static Config? instance = null;

        public static Config Data
        {
            get
            {
                if (instance == null)
                {
                    if (!Directory.Exists(DIRECTORY))
                    {
                        Directory.CreateDirectory(DIRECTORY);
                    }
                    if (File.Exists(FILE))
                    {
                        var jsonString = File.ReadAllText(FILE);
                        instance = JsonSerializer.Deserialize<Config>(jsonString);
                    }
                    if (instance == null)
                    {
                        instance = new Config();
                    }
                    else
                    {
                        instance.IsBlank = false;
                    }
                }
                return instance;
            }
        }

        public void Save()
        {
            string jsonString = JsonSerializer.Serialize<Config>(this);
            File.WriteAllText(FILE, jsonString);
        }
    }
}